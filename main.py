import cv2
import numpy as np
from pathlib import Path


def detector(img, clf, scale_factor=None, min_neighbors=None):
    result = img.copy()
    rects = clf.detectMultiScale(
        result,
        scaleFactor=scale_factor,
        minNeighbors=min_neighbors
    )

    for (x, y, w, h) in rects:
        cv2.rectangle(result, (x, y), (x+w, y+h), (255, 0, 0))

    return result


def detect_max_area_obj(img, clf, scale_factor=None, min_neighbors=None):
    result = img.copy()
    rects = clf.detectMultiScale(
        result,
        scaleFactor=scale_factor,
        minNeighbors=min_neighbors
    )

    if len(rects) > 0:
        max_area = float('-inf')
        x, y, w, h = 0, 0, 0, 0

        for (_x, _y, _w, _h) in rects:
            area = _w * _h

            if area > max_area:
                max_area = area
                x, y, w, h = _x, _y, _w, _h

        if max_area != float('-inf'):
            return (x, y, w, h)

    return None


def detect_eyes(img, clf, scale_factor=None, min_neighbors=None, epsilon=100):
    result = img.copy()
    rects = clf.detectMultiScale(
        result,
        scaleFactor=scale_factor,
        minNeighbors=min_neighbors
    )

    if len(rects) > 1:
        rects = sorted(rects, key=lambda rect: rect[1], reverse=True) # sort by `y`

        x1, y1, w1, h1 = rects[0]
        x2, y2, w2, h2 = rects[1]

        if abs(y1 + h1 / 2 - y2 + h2 / 2) < epsilon and h1 <= epsilon and h2 <= epsilon:
            lx, ly = np.minimum([x1, y1], [x2, y2])
            rx, ry = np.maximum([x1+w1, y1+h1], [x2+w2, y2+h2])

            return (lx, rx, ly, ry)        

    return None    


if __name__ == "__main__":

    cam = cv2.VideoCapture(0)

    face_cascade = Path(__file__).parent / "cascades" / "haarcascades" / "haarcascade_frontalface_default.xml"
    face_clf = cv2.CascadeClassifier(str(face_cascade))

    eye_cascade = Path(__file__).parent / "cascades" / "haarcascades" / "haarcascade_eye.xml"
    eye_clf = cv2.CascadeClassifier(str(eye_cascade))

    glasses = cv2.imread(str(Path(__file__).parent / "dealwithit.png"), cv2.IMREAD_UNCHANGED)

    scale_percent = 10 # percent of original size
    width = int(glasses.shape[1] * scale_percent / 100)
    height = int(glasses.shape[0] * scale_percent / 100)
    dim = (width, height)

    resized = cv2.resize(glasses, dim, interpolation = cv2.INTER_AREA)

    SCALE_FACTOR = 1.2
    MIN_NEIGHBORS = 5

    cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
    cv2.namedWindow("Face", cv2.WINDOW_KEEPRATIO)

    while cam.isOpened():
        key = cv2.waitKey(1)

        if key == ord('q'):
            break

        ret, frame = cam.read()
        cv2.flip(frame, 1, frame)
        cv2.GaussianBlur(frame, (9, 9), 0, frame)

        area = detect_max_area_obj(frame, face_clf, SCALE_FACTOR, MIN_NEIGHBORS)
        if area:
            x, y, w, h = area
            face = frame[y:y+h, x:x+w]
            eyes_coords = detect_eyes(face, eye_clf, SCALE_FACTOR, MIN_NEIGHBORS)

            if eyes_coords is not None:
                lfx, rfx, lfy, rfy = eyes_coords
                lfx, rfx, lfy, rfy = lfx + x, rfx + x, lfy + y, rfy + y
                # (width, height)
                dim = (rfx - lfx, rfy - lfy)
                resized_glasses = cv2.resize(glasses, dim, interpolation=cv2.INTER_NEAREST)

                frame[lfy:rfy, lfx:rfx] = frame[lfy:rfy, lfx:rfx] * (1 - resized_glasses[:,:,3:] / 255) + resized_glasses[:,:,:3] * (resized_glasses[:,:,3:] / 255)

            # cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0))

            cv2.imshow("Face", face)
        cv2.imshow("Camera", frame)

    cv2.destroyAllWindows()
    cam.release()